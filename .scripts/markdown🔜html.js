#!/usr/bin/env -S deno run --allow-read --allow-write
import { parse } from "https://deno.land/x/rusty_markdown@v0.2.1/mod.ts";

Deno.writeTextFileSync(Deno.args[0].replace(/md|markdown$/, "html"), `<!DOCTYPE html>
${ parse(Deno.readTextFileSync(Deno.args[0])).parsed }
`)
