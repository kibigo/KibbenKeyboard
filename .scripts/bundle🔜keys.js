#!/usr/bin/env -S deno run --allow-read --allow-write
Deno.writeTextFileSync(Deno.args[0].replace(/bundle$/, "keys"), Deno.readTextFileSync(`${ Deno.args[0] }/Contents/Resources/${ Deno.args[0].replace(/bundle$/, "keylayout") }`).split("\n").filter($ => !/<\?xml|&#x0*1?[0-9A-Za-z];/u.test($)).join("\n"))
