<xslt:stylesheet
	id="transform"
	version="1.0"
	xmlns:xslt="http://www.w3.org/1999/XSL/Transform"
	xmlns:html="http://www.w3.org/1999/xhtml"
>
	<xslt:variable name="keylayout" select="//html:link[@rel='alternate'][@type='application/xml']/@href[1]"/>
	<xslt:template match="html:*">
		<xslt:copy>
			<xslt:for-each select="@*">
				<xslt:copy/>
			</xslt:for-each>
			<xslt:apply-templates/>
		</xslt:copy>
	</xslt:template>
	<xslt:template match="html:head">
		<xslt:copy>
			<xslt:for-each select="@*">
				<xslt:copy/>
			</xslt:for-each>
			<xslt:apply-templates/>
			<html:style>
				<xslt:text>@namespace "http://www.w3.org/1999/xhtml";
figure.KEYBOARD{ Display: Grid; Margin: 1EM Auto; Width: 50EM; Grid: Auto-Flow / 1FR Max-Content; Gap: 2EM; Align-Items: Center }
figure.KEYBOARD figcaption{ Justify-Self: End; Font-Style: Italic; Text-Align: End; Text-Align-Last: Auto; }
figure.KEYBOARD table{ Box-Sizing: Border-Box; Table-Layout: Fixed; Margin: 0; Border: Thin #B8B3B0 Solid; Border-Spacing: .25EM; Width: 40EM; Color: #000000; Background: #FAF8F7; Font-Family: Sans-Serif }
figure.KEYBOARD col{ Width: .25EM }
figure.KEYBOARD td{ Position: Relative; Border-Radius: .125EM; Color: #DEDBD9; Background: #1F1A14; Line-Height: 1.5EM; Text-Align: Center }
figure.KEYBOARD td:Focus,
figure.KEYBOARD td:Hover{ Color: #FAF8F7; Background: #57534C }
figure.KEYBOARD td.ACTIVE{ Color: #FAF8F7; Background: #B01C1C }
figure.KEYBOARD td kbd{ Display: Block; Font-Family: Inherit }
figure.KEYBOARD td small{ Display: None; Position: Absolute; Top: 100%; Left: 50%; Right: 50%; Margin: 0 -3.5REM; Border-Radius: .125EM; Border: Thin #B8B3B0 Solid; Grid: Auto-Flow / Repeat(7, 1FR); Color: #000000; Background: #FAF8F7; Font-Size: Smaller; Z-Index: 1 }
figure.KEYBOARD td:Focus small,
figure.KEYBOARD td:Hover small{ Display: Grid }
figure.KEYBOARD td small samp{ Font-Family: Inherit }
figure.KEYBOARD td u{ Text-Decoration-Style: Dotted }</xslt:text>
			</html:style>
		</xslt:copy>
	</xslt:template>
	<xslt:template match="html:figure[@class='KEYBOARD'][@data-map-index]">
		<xslt:variable name="mapIndex" select="current()/@data-map-index"/>
		<xslt:variable name="caps" select="contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'caps') and not(contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'caps?'))"/>
		<xslt:variable name="anyShift" select="contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'anyShift') and not(contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'anyShift?'))"/>
		<xslt:variable name="anyOption" select="contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'anyOption') and not(contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'anyOption?'))"/>
		<xslt:variable name="command" select="contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'command') and not(contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'command?'))"/>
		<xslt:variable name="anyControl" select="contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'anyControl') and not(contains(document($keylayout)//keyMapSelect[@mapIndex=$mapIndex]/modifier/@keys, 'anyControl?'))"/>
		<xslt:copy>
			<xslt:for-each select="@*">
				<xslt:copy/>
			</xslt:for-each>
			<html:figcaption>
				<xslt:value-of select="document($keylayout)//html:text"/>
				<xslt:choose>
					<xslt:when test="$command">Command</xslt:when>
					<xslt:when test="$caps and $anyShift and $anyOption">Caps<html:wbr/><html:span style="White-Space: NoWrap"> &amp; </html:span>Shift<html:wbr/><html:span style="White-Space: NoWrap"> &amp; </html:span>Option</xslt:when>
					<xslt:when test="$caps and $anyOption">Caps<html:wbr/><html:span style="White-Space: NoWrap"> &amp; </html:span>Option</xslt:when>
					<xslt:when test="$caps and $anyShift">Caps<html:wbr/><html:span style="White-Space: NoWrap"> &amp; </html:span>Shift</xslt:when>
					<xslt:when test="$caps">Caps</xslt:when>
					<xslt:when test="$anyShift and $anyOption">Shift<html:wbr/><html:span style="White-Space: NoWrap"> &amp; </html:span>Option</xslt:when>
					<xslt:when test="$anyOption">Option</xslt:when>
					<xslt:when test="$anyShift">Shift</xslt:when>
					<xslt:otherwise>No modifiers</xslt:otherwise>
				</xslt:choose>
			</html:figcaption>
			<xslt:apply-templates/>
			<html:table>
				<html:colgroup>
					<html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/><html:col/>
				</html:colgroup>
				<html:tbody>
					<html:tr>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ISO_Section'"/>
								<xslt:with-param name="code" select="'10'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_1'"/>
								<xslt:with-param name="code" select="'18'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_2'"/>
								<xslt:with-param name="code" select="'19'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_3'"/>
								<xslt:with-param name="code" select="'20'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_4'"/>
								<xslt:with-param name="code" select="'21'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_5'"/>
								<xslt:with-param name="code" select="'23'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_6'"/>
								<xslt:with-param name="code" select="'22'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_7'"/>
								<xslt:with-param name="code" select="'26'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_8'"/>
								<xslt:with-param name="code" select="'28'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_9'"/>
								<xslt:with-param name="code" select="'25'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_0'"/>
								<xslt:with-param name="code" select="'29'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Minus'"/>
								<xslt:with-param name="code" select="'27'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Equal'"/>
								<xslt:with-param name="code" select="'24'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="6" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Delete'"/>
								<xslt:with-param name="code" select="'51'"/>
								<xslt:with-param name="output" select="'⌦'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadClear'"/>
								<xslt:with-param name="code" select="'71'"/>
								<xslt:with-param name="output" select="'⌧'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadEquals'"/>
								<xslt:with-param name="code" select="'81'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadDivide'"/>
								<xslt:with-param name="code" select="'75'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadMultiply'"/>
								<xslt:with-param name="code" select="'67'"/>
							</xslt:call-template>
						</html:td>
					</html:tr>
					<html:tr>
						<html:td colspan="6" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Tab'"/>
								<xslt:with-param name="code" select="'48'"/>
								<xslt:with-param name="output" select="'⇥'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Q'"/>
								<xslt:with-param name="code" select="'12'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_W'"/>
								<xslt:with-param name="code" select="'13'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_E'"/>
								<xslt:with-param name="code" select="'14'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_R'"/>
								<xslt:with-param name="code" select="'15'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_T'"/>
								<xslt:with-param name="code" select="'17'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Y'"/>
								<xslt:with-param name="code" select="'16'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_U'"/>
								<xslt:with-param name="code" select="'32'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_I'"/>
								<xslt:with-param name="code" select="'34'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_O'"/>
								<xslt:with-param name="code" select="'31'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_P'"/>
								<xslt:with-param name="code" select="'35'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_LeftBracket'"/>
								<xslt:with-param name="code" select="'33'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_RightBracket'"/>
								<xslt:with-param name="code" select="'30'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Backslash'"/>
								<xslt:with-param name="code" select="'42'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad7'"/>
								<xslt:with-param name="code" select="'89'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad8'"/>
								<xslt:with-param name="code" select="'91'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad9'"/>
								<xslt:with-param name="code" select="'92'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadMinus'"/>
								<xslt:with-param name="code" select="'78'"/>
							</xslt:call-template>
						</html:td>
					</html:tr>
					<html:tr>
						<html:td colspan="7" tabindex="-1">
							<xslt:if test="$caps">
								<xslt:attribute name="class">ACTIVE</xslt:attribute>
							</xslt:if>
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_CapsLock'"/>
								<xslt:with-param name="code" select="'57'"/>
								<xslt:with-param name="output" select="'⇪'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_A'"/>
								<xslt:with-param name="code" select="'0'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_S'"/>
								<xslt:with-param name="code" select="'1'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_D'"/>
								<xslt:with-param name="code" select="'2'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_F'"/>
								<xslt:with-param name="code" select="'3'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_G'"/>
								<xslt:with-param name="code" select="'5'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_H'"/>
								<xslt:with-param name="code" select="'4'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_J'"/>
								<xslt:with-param name="code" select="'38'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_K'"/>
								<xslt:with-param name="code" select="'40'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_L'"/>
								<xslt:with-param name="code" select="'37'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Semicolon'"/>
								<xslt:with-param name="code" select="'41'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Quote'"/>
								<xslt:with-param name="code" select="'39'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="7" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Return'"/>
								<xslt:with-param name="code" select="'36'"/>
								<xslt:with-param name="output" select="'⏎'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad4'"/>
								<xslt:with-param name="code" select="'86'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad5'"/>
								<xslt:with-param name="code" select="'87'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad6'"/>
								<xslt:with-param name="code" select="'88'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadPlus'"/>
								<xslt:with-param name="code" select="'69'"/>
							</xslt:call-template>
						</html:td>
					</html:tr>
					<html:tr>
						<html:td colspan="5" tabindex="-1">
							<xslt:if test="$anyShift">
								<xslt:attribute name="class">ACTIVE</xslt:attribute>
							</xslt:if>
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Shift'"/>
								<xslt:with-param name="code" select="'56'"/>
								<xslt:with-param name="output" select="'⇧'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Grave'"/>
								<xslt:with-param name="code" select="'50'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Z'"/>
								<xslt:with-param name="code" select="'6'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_X'"/>
								<xslt:with-param name="code" select="'7'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_C'"/>
								<xslt:with-param name="code" select="'8'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_V'"/>
								<xslt:with-param name="code" select="'9'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_B'"/>
								<xslt:with-param name="code" select="'11'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_N'"/>
								<xslt:with-param name="code" select="'45'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_M'"/>
								<xslt:with-param name="code" select="'46'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Comma'"/>
								<xslt:with-param name="code" select="'43'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Period'"/>
								<xslt:with-param name="code" select="'47'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Slash'"/>
								<xslt:with-param name="code" select="'44'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="9" tabindex="-1">
							<xslt:if test="$anyShift">
								<xslt:attribute name="class">ACTIVE</xslt:attribute>
							</xslt:if>
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Shift'"/>
								<xslt:with-param name="code" select="'56'"/>
								<xslt:with-param name="output" select="'⇧'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad1'"/>
								<xslt:with-param name="code" select="'83'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad2'"/>
								<xslt:with-param name="code" select="'84'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad3'"/>
								<xslt:with-param name="code" select="'85'"/>
							</xslt:call-template>
						</html:td>
						<html:td rowspan="2" colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadEnter'"/>
								<xslt:with-param name="code" select="'76'"/>
								<xslt:with-param name="output" select="'⌅'"/>
							</xslt:call-template>
						</html:td>
					</html:tr>
					<html:tr>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Function'"/>
								<xslt:with-param name="code" select="'63'"/>
								<xslt:with-param name="output" select="'🌐'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:if test="$anyControl">
								<xslt:attribute name="class">ACTIVE</xslt:attribute>
							</xslt:if>
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Control'"/>
								<xslt:with-param name="code" select="'59'"/>
								<xslt:with-param name="output" select="'⎈'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:if test="$anyOption">
								<xslt:attribute name="class">ACTIVE</xslt:attribute>
							</xslt:if>
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Option'"/>
								<xslt:with-param name="code" select="'58'"/>
								<xslt:with-param name="output" select="'⌥'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="5" tabindex="-1">
							<xslt:if test="$command">
								<xslt:attribute name="class">ACTIVE</xslt:attribute>
							</xslt:if>
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Command'"/>
								<xslt:with-param name="code" select="'55'"/>
								<xslt:with-param name="output" select="'⌘'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="20" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Space'"/>
								<xslt:with-param name="code" select="'49'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="5" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_Escape'"/>
								<xslt:with-param name="code" select="'53'"/>
								<xslt:with-param name="output" select="'⎋'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_LeftArrow'"/>
								<xslt:with-param name="code" select="'123'"/>
								<xslt:with-param name="output" select="'←'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_RightArrow'"/>
								<xslt:with-param name="code" select="'124'"/>
								<xslt:with-param name="output" select="'→'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_DownArrow'"/>
								<xslt:with-param name="code" select="'125'"/>
								<xslt:with-param name="output" select="'↓'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_UpArrow'"/>
								<xslt:with-param name="code" select="'126'"/>
								<xslt:with-param name="output" select="'↑'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="8" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_Keypad0'"/>
								<xslt:with-param name="code" select="'82'"/>
							</xslt:call-template>
						</html:td>
						<html:td colspan="4" tabindex="-1">
							<xslt:call-template name="key">
								<xslt:with-param name="mapIndex" select="$mapIndex"/>
								<xslt:with-param name="title" select="'kVK_ANSI_KeypadDecimal'"/>
								<xslt:with-param name="code" select="'65'"/>
							</xslt:call-template>
						</html:td>
					</html:tr>
				</html:tbody>
			</html:table>
		</xslt:copy>
	</xslt:template>
	<xslt:template name="key">
		<xslt:param name="mapIndex" select="'0'"/>
		<xslt:param name="code" select="'??'"/>
		<xslt:param name="title" select="''"/>
		<xslt:param name="output" select="false"/>
		<xslt:choose>
			<xslt:when test="$output">
				<html:kbd title="{$code}: {$title}">
					<xslt:value-of select="$output"/>
				</html:kbd>
			</xslt:when>
			<xslt:otherwise>
				<xslt:for-each select="document($keylayout)//keyMapSet/keyMap[@index=$mapIndex]/key[@code=$code]">
					<xslt:choose>
						<xslt:when test="@output">
							<html:kbd title="{$code}: {$title}">
								<xslt:value-of select="@output"/>
							</html:kbd>
						</xslt:when>
						<xslt:otherwise>
							<xslt:for-each select="document($keylayout)//action[@id=current()/@action]/when[@state='none']">
								<html:kbd title="{$code}: {$title}">
									<xslt:choose>
										<xslt:when test="@output">
											<xslt:value-of select="@output"/>
										</xslt:when>
										<xslt:otherwise>
											<html:u>
												<xslt:value-of select="document($keylayout)//terminators/when[@state=current()/@next]/@output"/>
											</html:u>
										</xslt:otherwise>
									</xslt:choose>
								</html:kbd>
								<xslt:if test="../when[@state!='none'][@output]">
									<html:small>
										<xslt:for-each select="../when[@state!='none'][@output]">
											<html:samp>
												<xslt:value-of select="@output"/>
											</html:samp>
										</xslt:for-each>
									</html:small>
								</xslt:if>
							</xslt:for-each>
						</xslt:otherwise>
					</xslt:choose>
				</xslt:for-each>
			</xslt:otherwise>
		</xslt:choose>
	</xslt:template>
</xslt:stylesheet>
