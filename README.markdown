#  ⌨️❤️ Kibben Keyboard  #

This repository contains a handwritten macOS Keyboard Bundle for the input of English text, with support for a wide variety of special characters and diacritics.

You can preview the keyboard layout online at <https://kibigo.gitlab.io/KibbenKeyboard/>.


##  Installation  ##

01. Clone [this repository](https://gitlab.com/kibigo/KibbenKeyboard), or download it as a Zip and unzip its contents.

02. In Finder, click `Go > Go to Folder…` and type `~/Library`, then press Return to navigate to your user library folder.
Alternatively, type `/Library` to install the keyboard layout globally (for all users).

03. Create the `Keyboard Layouts` directory if it doesn't already exist in this location.

04. Drag `Kibben.bundle` (from this repository) into the `Keyboard Layouts` folder.

05. Restart your computer.


##  Usage  ##

Open System Preferences, click on Keyboard, and then Input Sources.
Use the + button to add a new input source.
The Kibben keyboard will be available under the English category.


##  Uninstalling  ##

01. Remove “Kibben” from your list of input sources in System Preferences, if present.

02. Delete `Kibben.bundle` from the directory you installed it into (as above).

03. Restart your computer.


##  Updating  ##

Uninstall, then install again.
You probably will need to restart your computer inbetween uninstalling and reinstalling.


##  Notes  ##

 +  To output standalone (spacing) diacritics, type the corresponding dead key and press space.

 +  Caps Lock switches the keyboard into numeral/symbol input.
    Alphabetic characters are replaced with Greek, numbers are replaced with shapes, and other punctuation is replaced with Roman numerals.
    When Option is held down, a number of additional symbol characters are made available.

 +  Option+Space produces a nonbreaking thin space, for use when padding punctuation.
    Shift+Option+Space produces an ordinary nonbreaking space.
    So, be sure to release Option if your intent is to type a breaking interword space.


##  Changelog  ##

 +  **6.6:**
    Replaces `@—` with `¦@` and adds `:⁠—` and `—⁠:` as single keys.
    Adds quist (ogonek above) and (over)ring as additional combining marks.
    Moves `—⸺` to be on B in place of the lozenge.
    Replaces `⁺` with `⁜`.
    Adds `ƛ`, with `Ƛ` as its tentative uppercase, in place of `ɂɁ`.
    Further shifting and replacement of symbols as necessary.

 +  **6.5:**
    Switches `V@Z` and `PYM«‘·»’—B` on all levels.
    Further adjusts some punctuation.
    Reorganizes the Greek.
    Adds pound `£` as capital `ſ` and moves peso `$` onto P.
    Adds glottal stop `Ɂɂ`.
    Replaces not `¬` with yen `¥`.

 +  **6.4:**
    Switches `DJU` and `TK` on all levels.

 +  **6.3:**
    Switches `DH` and `TG` on all levels.
    (Re)adds yogh `Ȝȝ` and numero `№`.
    Adds Greek letter sho `Ϸϸ`.
    Adds gender and sexuality symbols.
    Rearranges numpad for ease·of·use on devices without a numpad equals.
    Rearranges symbols and adds the rating symbols `🗡💋🗣`.

 +  **6.2:**
    Switches `AS` and `LR` on all levels.

 +  **6.1:**
    Rearranges `CXVZW` for better ergonomics.
    `@` and `` ` `` swapped so that the latter is now on shift.

 +  **6.0:**
    No longer a QWERTY keyboard; keys have been shifted to make higher levels of keyboard function more sensible.
    Tironian et `⹒⁊` and long S `ſ` replace yen `¥` and cent `¢`.
    Keypad now varies on Shift/Option/Caps and has been adjusted to resemble phone layouts.
    Significant shifting of symbols; lock symbols (`🔒🔓`) have been added.

 +  **5.2:**
    `🔚` was mistakenly included twice; the new one has now been replaced with `📌`.

 +  **5.1:**
    Divides has been added and vertical bar moved to shift, for parity with the solidus operators.
    The I·S·O “extra key” now has defined mappings.
    Symbols (`📖🎼🖼🎭🎥🎮`) have been added on Caps+Option for identifying different forms of media.

 +  **5.0:**
    A·S·C·I·I on Caps+Option has been replaced with a symbol keyboard.
    Counting rod numerals have been removed and replaced with a number of shapes (in both white and black).
    Division slash and reverse solidus operator have been added, which are proper operator forms of solidus and reverse solidus.
    Dottless I and J have been removed and replaced with curving arrows.
    The number of dead keys has been reduced and their default output has changed.
    A number of other symbols have been modified or shuffled around for usability.

 +  **4.3:**
    Now uses actual Unicode Roman numeral characters (`ⅠⅤⅩⅬⅭⅮⅯ`) for Roman numerals, as the distinction is sometimes useful.

 +  **4.2:**
    Swaps double hyphen/lozenge (previously shift+option) and equals/comparable (previously option) to make typing things such as `a != -1` easier.

 +  **4.1:**
    Adds double hyphen.

 +  **4.0:**
    Removes most I·P·A letters, as well as Runic characters (which never got much use).
    Adds additional symbols and numerals (including Greek) in their place.

 +  **3.6:**
    Replaces duplicate pound sign with missing double vertical bar.

 +  **3.5:**
    Replaces duplicate reverse empty set with missing dotted cross.

 +  **3.4:**
    Adds dagger as the Latin equivalent of Runic cross.

 +  **3.3:**
    Fixes missing underbar.
    Rearranges number row symbols for ease of use.
    Removes bars with quill, and adds registered and copyright signs instead.

 +  **3.2:**
    Adds missing numpad period.
    Swaps single anglequotes and quotation dash.
    Numbers are now shifted, with 0 appearing at QWERTY 1, and some rearrangement of special characters to match.

 +  **3.1:**
    Replaces forward‐emptyset with ounce sign, usable as an indicator of dozenal numbers.
    This is not a maths layout and reverse‐emptyset is more suitable for Kibben.

 +  **3.0:**
    New updated Kibben keyboard layout, with a great deal many more diacritics and fewer obscure Latin characters.
    More complete I·P·A support and repositioning of symbols.

 +  **2.2:**
    Fixes tilde behaviour.
    Adds missing Open E characters with a slight rearrangement of the keyboard.
    Removes automatic insertion of narrow spaces around punctuation.
    Replaces raised omission brackets with dead keys for grave and acute accents.

 +  **2.1:**
    Provides remaining A·S·C·I·I characters in the non‐capslocked keyboard and moves around some characters for easier access.

 +  **2.0:**
    Rename to “Kibben”/“Kybn”.
    Basically an entirely new keyboard layout, with full support for the Kibben character set, including runes.
    The vast majority of diacritic support has been dropped, as have most alternate or historical glyphs.
    The former can typically be accessed through the macOS press‐and‐hold system, and the latter are unlikely to see enough use to justify a position in the keyboard.

 +  **1.1:**
    Bugfix for capital KRA.

 +  **1.0:**
    Initial release.
